# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $("#user_role").change ->
    if this.value == '0'
      $(".sales_fields").removeClass('d-none')
    if this.value == '1'
      $(".sales_fields").addClass('d-none')
    
  $('#search_users').on 'keyup', ->
    value = $(this).val().toLowerCase()
    $('#users_table tbody tr').filter ->
      $(this).toggle $(this).text().toLowerCase().indexOf(value) > -1
      return
    return
