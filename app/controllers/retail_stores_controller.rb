class RetailStoresController < ApplicationController
  load_and_authorize_resource
  before_action :set_retail_store, only: [:show, :edit, :update, :destroy]

  # GET /retail_stores
  # GET /retail_stores.json
  def index
    @retail_stores = RetailStore.all
  end

  # GET /retail_stores/1
  # GET /retail_stores/1.json
  def show
  end

  # GET /retail_stores/new
  def new
    @retail_store = RetailStore.new
  end

  # GET /retail_stores/1/edit
  def edit
  end

  # POST /retail_stores
  # POST /retail_stores.json
  def create
    @retail_store = RetailStore.new(retail_store_params)

    respond_to do |format|
      if @retail_store.save
        format.html { redirect_to @retail_store, notice: 'Retail store was successfully created.' }
        format.json { render :show, status: :created, location: @retail_store }
      else
        format.html { render :new }
        format.json { render json: @retail_store.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /retail_stores/1
  # PATCH/PUT /retail_stores/1.json
  def update
    respond_to do |format|
      if @retail_store.update(retail_store_params)
        format.html { redirect_to @retail_store, notice: 'Retail store was successfully updated.' }
        format.json { render :show, status: :ok, location: @retail_store }
      else
        format.html { render :edit }
        format.json { render json: @retail_store.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /retail_stores/1
  # DELETE /retail_stores/1.json
  def destroy
    @retail_store.destroy
    respond_to do |format|
      format.html { redirect_to retail_stores_url, notice: 'Retail store was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_retail_store
      @retail_store = RetailStore.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def retail_store_params
      params.require(:retail_store).permit(:name, :location)
    end
end
