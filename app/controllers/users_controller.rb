class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :set_user , only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @users = User.all.order('email ASC')
  end
  
  def new
    @user = User.new

    @user.build_sales_rep

    authorize! :new, @user
    respond_with @user
  end

  def show
    return redirect_to root_url unless current_user.admin?
    authorize! :show, @user
    respond_with @user
  end

  def edit
    authorize! :edit, @user

    @user.build_sales_rep if @user.sales_rep.nil?

    respond_with @user
  end

  def create
    @user = User.new(user_params)
    authorize! :create, @user

    if @user.save
      redirect_to users_path, :notice => 'User added successfully'
    else
      respond_to do |format|
        format.json { render :text => "Could not create user", :status => :unprocessable_entity }
        format.html do
          flash[:notice] = "Could not create user"
          render :action => :new, :status => :unprocessable_entity
        end
      end
    end
  end

  def update
    authorize! :update, @user

    new_params = user_params
    if user_params[:password].empty? and user_params[:password_confirmation].empty?
      new_params.delete :password 
      new_params.delete :password_confirmation 
    end

    if @user.update(new_params)
      redirect_to edit_user_path(@user), :notice => 'User details updated successfully'
    else
      render :action => :edit
    end
  end

  def destroy
    if @user == current_user
      flash[:error] = 'You may not delete yourself'
    else
      authorize! :destroy, @user
      @user.destroy
    end
    redirect_to users_path
  end

  private
  def set_user
    if params[:id].nil?
      @user = current_user
    else
      User.find(params[:id])
    end
  end

  def user_params
    p = [:email, :password, :password_confirmation, sales_rep_attributes: [:id, :name, :employment_start_date] ]

    if current_user.admin? # only admin can change this
      p += [:role]
    end

    params.require(:user).permit(p)
  end
end
