class StoreVisitsController < ApplicationController
  load_and_authorize_resource
  before_action :set_store_visit, only: [:show, :edit, :update, :destroy]

  # GET /store_visits
  # GET /store_visits.json
  def index
    if current_user.admin?
      @store_visits = StoreVisit.all
    else
      @store_visits = current_user.sales_rep.store_visits
    end
  end

  # GET /store_visits/1
  # GET /store_visits/1.json
  def show
  end

  # GET /store_visits/new
  def new
    @store_visit = StoreVisit.new

    @store_visit.product_verifications.build
  end

  # GET /store_visits/1/edit
  def edit
  end

  # POST /store_visits
  # POST /store_visits.json
  def create
    @store_visit = StoreVisit.new(store_visit_params)

    respond_to do |format|
      if @store_visit.save
        format.html { redirect_to @store_visit, notice: 'Store visit was successfully created.' }
        format.json { render :show, status: :created, location: @store_visit }
      else
        format.html { render :new }
        format.json { render json: @store_visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /store_visits/1
  # PATCH/PUT /store_visits/1.json
  def update
    respond_to do |format|
      if @store_visit.update(store_visit_params)
        format.html { redirect_to @store_visit, notice: 'Store visit was successfully updated.' }
        format.json { render :show, status: :ok, location: @store_visit }
      else
        format.html { render :edit }
        format.json { render json: @store_visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /store_visits/1
  # DELETE /store_visits/1.json
  def destroy
    @store_visit.destroy
    respond_to do |format|
      format.html { redirect_to store_visits_url, notice: 'Store visit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_visit
      @store_visit = StoreVisit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def store_visit_params
      params.require(:store_visit).permit(:retail_store_id, :sales_rep_id, :verification_date, product_verifications_attributes: [:id, :product_id, :unit_count])
    end
end