class StoreVisit < ApplicationRecord
  belongs_to :sales_rep
  belongs_to :retail_store
  has_many :product_verifications

  accepts_nested_attributes_for :product_verifications, allow_destroy: true
end
