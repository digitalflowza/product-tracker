class SalesRep < ApplicationRecord
  has_many :retail_stores
  has_many :store_visits

  belongs_to :user
end
