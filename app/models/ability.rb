# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    case user.role
      when User::ROLES[:admin]
        can :manage, :all
      when User::ROLES[:sales]
        #can :manage, User, :id => user.id
        can :manage, :all
    end
  end
end
