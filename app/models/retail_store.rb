class RetailStore < ApplicationRecord
  has_many :store_visits
  belongs_to :sales_rep, optional: true
end
