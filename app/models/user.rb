class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  has_one :sales_rep, :dependent => :destroy
  accepts_nested_attributes_for :sales_rep

  ROLES = {
    :sales => 0, :admin => 1
  }

  def admin?
    role == ROLES[:admin]
  end
end
