class CreateProductVerifications < ActiveRecord::Migration[5.2]
  def change
    create_table :product_verifications do |t|
      t.integer :product_id
      t.integer :unit_count
      t.integer :store_visit_id

      t.timestamps
    end
  end
end
