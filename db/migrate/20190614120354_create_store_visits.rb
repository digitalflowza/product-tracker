class CreateStoreVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :store_visits do |t|
      t.integer :retail_store_id
      t.integer :sales_rep_id

      t.timestamps
    end
  end
end
