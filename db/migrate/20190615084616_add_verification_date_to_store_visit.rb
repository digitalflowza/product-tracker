class AddVerificationDateToStoreVisit < ActiveRecord::Migration[5.2]
  def change
    add_column :store_visits, :verification_date, :date
  end
end
