class CreateSalesReps < ActiveRecord::Migration[5.2]
  def change
    create_table :sales_reps do |t|
      t.string :name
      t.datetime :employment_start_date
      t.integer :user_id

      t.timestamps
    end
  end
end
