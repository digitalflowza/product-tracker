class ChangeSalesRepStartDate < ActiveRecord::Migration[5.2]
  def change
    change_column :sales_reps, :employment_start_date, :date
  end
end
