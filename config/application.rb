require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ProductTracker
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.assets.enabled = true

    config.to_prepare do
      Devise::SessionsController.layout "authentication"
      Devise::RegistrationsController.layout proc{ |controller| user_signed_in? ? "application" : "authentication" }
      Devise::ConfirmationsController.layout "authentication"
      Devise::UnlocksController.layout "authentication"
      Devise::PasswordsController.layout "authentication"
    end

    config.encoding = "utf-8"
    config.time_zone = 'UTC'

    config.generators do |g|
      g.template_engine :haml
    end
  end
end
