Rails.application.routes.draw do
  resources :store_visits
  resources :products
  resources :retail_stores
  resources :sales_reps
  devise_for :users
  resources :users

  root to: 'page#index'
end
