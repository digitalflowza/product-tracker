require "application_system_test_case"

class StoreVisitsTest < ApplicationSystemTestCase
  setup do
    @store_visit = store_visits(:one)
  end

  test "visiting the index" do
    visit store_visits_url
    assert_selector "h1", text: "Store Visits"
  end

  test "creating a Store visit" do
    visit store_visits_url
    click_on "New Store Visit"

    fill_in "Retail store", with: @store_visit.retail_store_id
    fill_in "Sales rep", with: @store_visit.sales_rep_id
    click_on "Create Store visit"

    assert_text "Store visit was successfully created"
    click_on "Back"
  end

  test "updating a Store visit" do
    visit store_visits_url
    click_on "Edit", match: :first

    fill_in "Retail store", with: @store_visit.retail_store_id
    fill_in "Sales rep", with: @store_visit.sales_rep_id
    click_on "Update Store visit"

    assert_text "Store visit was successfully updated"
    click_on "Back"
  end

  test "destroying a Store visit" do
    visit store_visits_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Store visit was successfully destroyed"
  end
end
