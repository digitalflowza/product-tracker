require "application_system_test_case"

class SalesRepsTest < ApplicationSystemTestCase
  setup do
    @sales_rep = sales_reps(:one)
  end

  test "visiting the index" do
    visit sales_reps_url
    assert_selector "h1", text: "Sales Reps"
  end

  test "creating a Sales rep" do
    visit sales_reps_url
    click_on "New Sales Rep"

    fill_in "Employment start date", with: @sales_rep.employment_start_date
    fill_in "Name", with: @sales_rep.name
    fill_in "User", with: @sales_rep.user_id
    click_on "Create Sales rep"

    assert_text "Sales rep was successfully created"
    click_on "Back"
  end

  test "updating a Sales rep" do
    visit sales_reps_url
    click_on "Edit", match: :first

    fill_in "Employment start date", with: @sales_rep.employment_start_date
    fill_in "Name", with: @sales_rep.name
    fill_in "User", with: @sales_rep.user_id
    click_on "Update Sales rep"

    assert_text "Sales rep was successfully updated"
    click_on "Back"
  end

  test "destroying a Sales rep" do
    visit sales_reps_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sales rep was successfully destroyed"
  end
end
