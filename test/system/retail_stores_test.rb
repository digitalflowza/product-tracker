require "application_system_test_case"

class RetailStoresTest < ApplicationSystemTestCase
  setup do
    @retail_store = retail_stores(:one)
  end

  test "visiting the index" do
    visit retail_stores_url
    assert_selector "h1", text: "Retail Stores"
  end

  test "creating a Retail store" do
    visit retail_stores_url
    click_on "New Retail Store"

    fill_in "Location", with: @retail_store.location
    fill_in "Name", with: @retail_store.name
    click_on "Create Retail store"

    assert_text "Retail store was successfully created"
    click_on "Back"
  end

  test "updating a Retail store" do
    visit retail_stores_url
    click_on "Edit", match: :first

    fill_in "Location", with: @retail_store.location
    fill_in "Name", with: @retail_store.name
    click_on "Update Retail store"

    assert_text "Retail store was successfully updated"
    click_on "Back"
  end

  test "destroying a Retail store" do
    visit retail_stores_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Retail store was successfully destroyed"
  end
end
